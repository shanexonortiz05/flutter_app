import 'dart:math';

import 'package:flutter/material.dart';
import 'package:food_addresses/providers/Card_info_provider.dart';
import 'package:food_addresses/providers/Order_provider.dart';
import 'package:food_addresses/providers/login_provider.dart';
import 'package:food_addresses/utils/Colors_utils.dart';
import 'package:food_addresses/utils/images_util.dart';
import 'package:provider/provider.dart';

class Card_product extends StatelessWidget {
  const Card_product({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final data = Provider.of<Orders_Data>(context);

    return _list(context);
  }

  Widget _list(context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    CardsProvider C = new CardsProvider();
    final token = Provider.of<Tok>(context, listen: false);
    final data = Provider.of<Orders_Data>(context, listen: false);
    return FutureBuilder(
        future: C.cargar(token.oid),
        initialData: [],
        builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.active:
              return Container();
            case ConnectionState.waiting:
              return Container(
                height: queryData.size.height - 200,
                child: data.allProducts > 0
                    ? ListView(children: card(context))
                    : Center(child: CircularProgressIndicator()),
                        
              );
            case ConnectionState.none:
              return Center(
                child: Text("no tienes niguna orden"),
              );
            case ConnectionState.done:
              if (snapshot.data.isEmpty) {
                return Container(
                  height: queryData.size.height - 200,
                  child: Center(
                      child: Text(
                    "No tienes niguna orden",
                    style: TextStyle(fontSize: 25),
                  )),
                );
              } else {
                data.reset();
                data.Cargar_data(snapshot.data);
                return Container(
                  height: queryData.size.height - 200,
                  child: ListView(children: card(context)),
                );
              }
          }
        });
  }

  List<Widget> card(context) {
    final data = Provider.of<Orders_Data>(context, listen: false);
    List<Widget> all = [];
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    var randomizer = new Random();
    int tam = data.allProducts;

    for (int i = 0; i < tam; i++) {
      data.Pos = i;
      final Temp = Container(
        margin: EdgeInsets.all(5),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Color(ColorsBg[randomizer.nextInt(ColorsBg.length)])),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    height: queryData.size.height / 4.5,
                    alignment: Alignment.centerLeft,
                    child: getimagenes('food')),
                Spacer(),
                Container(
                  //color: Colors.black,
                  width: queryData.size.width / 1.8,
                  padding: EdgeInsets.only(left: 0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        data.Usuario,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                      Text(
                        '\$' + ' ' + data.Total,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.7), fontSize: 15),
                      ),
                      InkWell(
                          onTap: () {
                            data.Pos = i;
                            Navigator.pushNamed(context, 'orderDetail');
                          },
                          child: Container(
                            margin: EdgeInsets.only(top: 20),
                            padding: EdgeInsets.all(4),
                            decoration: BoxDecoration(
                              color: Colors.blueAccent[100],
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: Icon(
                              Icons.arrow_forward,
                              size: 30,
                              color: Colors.white,
                            ),
                            //  child: Text(
                            //   'Detalle',
                            //   style: TextStyle(
                            //      color: Colors.black,
                            //      fontSize: 25,
                            //      fontWeight: FontWeight.w400),
                            // ),
                          )),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      );
      all.add(Temp);
    }
    ;

    return all;
  }
}
