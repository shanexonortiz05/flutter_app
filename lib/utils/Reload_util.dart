
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:food_addresses/providers/Card_info_provider.dart';
import 'package:food_addresses/providers/login_provider.dart';
import 'package:provider/provider.dart';

class Reload with ChangeNotifier{

  void reload(context) async{
    final token = Provider.of<Tok>(context, listen: false);
    CardsProvider C = new CardsProvider();
    await C.cargar(token.oid);
  }

}