import 'package:flutter/material.dart';
import 'package:food_addresses/providers/Order_provider.dart';
import 'package:food_addresses/providers/Product_povider.dart';
import 'package:food_addresses/providers/login_provider.dart';
import 'package:food_addresses/providers/notificate_provider.dart';
import 'package:food_addresses/routes/routes.dart';
import 'package:food_addresses/theme/theme.dart';
import 'package:food_addresses/utils/Reload_util.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => Product()),
        ChangeNotifierProvider(create: (_) => Tok()),
        ChangeNotifierProvider(create: (_) => Orders_Data()),
        ChangeNotifierProvider(create: (_) => Reload()),

      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Pedidos App',
        theme: myTheme,
        initialRoute: 'Page',
        routes: getApplicationRoutes(),
      ),
    );
  }

}
