
import 'package:flutter/material.dart';
import 'package:food_addresses/pages/Orders_page.dart';
import 'package:food_addresses/pages/Page.dart';
import 'package:food_addresses/pages/detail_order.dart';
import 'package:food_addresses/pages/log_page.dart';

Map<String, WidgetBuilder> getApplicationRoutes(){

  return<String, WidgetBuilder>{
      'orderDetail': (BuildContext context) => OrderDetail(),
      'log': (BuildContext context) => Log(),
      'orders': (BuildContext context) => OrdersPage(),
      'Page': (BuildContext context) => Pages()

  };

}

