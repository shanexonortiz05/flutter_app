import 'package:flutter/material.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:food_addresses/providers/Card_info_provider.dart';
import 'package:food_addresses/providers/Order_provider.dart';
import 'package:food_addresses/providers/Product_povider.dart';
import 'package:food_addresses/providers/coords_provider.dart';
import 'package:provider/provider.dart';
import 'package:map_launcher/map_launcher.dart';

class OrderDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final data = Provider.of<Orders_Data>(context, listen: false);
    TextStyle style = TextStyle(
        color: Colors.black.withOpacity(0.7),
        fontWeight: FontWeight.w400,
        fontSize: 20);

    return Scaffold(
      backgroundColor: Colors.white,
      floatingActionButton: FloatingActionButton(
          heroTag: "btn_1",
          backgroundColor: Colors.black87,
          child: Icon(Icons.assignment_turned_in, color: Colors.white),
          onPressed: () {
            _mostrarAlert(context, data.Id);
          }),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Image.asset(
          'assets/logo.png',
          height: 80,
        ),
        leading: BackButton(
            onPressed: () {
              data.reset();
              Navigator.popAndPushNamed(context, 'orders');
            },
            color: Colors.black),
      ),
      body: ListView(
        //fit: StackFit.expand,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(vertical: 35),
                color: Colors.white,
                height: 250,
                child: Image.asset(
                  'assets/3690353.png',
                  //  height: 80,
                ),
              )
            ],
          ),
          Container(
              margin: EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                children: <Widget>[
                  Divider(
                    height: 2,
                  ),
                  Text(data.Usuario,
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.w700)),
                  SizedBox(
                    height: 15,
                  ),
                  ListBody(
                    children: _products(context),
                  ),
                  Row(
                    children: <Widget>[
                      SizedBox(
                        width: 10,
                      ),
                      Icon(Icons.calendar_today,
                          size: 25, color: Colors.blueAccent[100]),
                      SizedBox(
                        width: 30,
                      ),
                      Text(
                        data.Fecha,
                        style: style,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.pin_drop,
                              size: 25, color: Colors.blueAccent[100]),
                          onPressed: () async {
                            CoordsAdrres C = new CoordsAdrres();

                            C.coord(data.Direccion).then((value) async {
                              final availableMaps =
                                  await MapLauncher.installedMaps;
                              await availableMaps.first.showMarker(
                                coords: Coords(value[0]['location']['y'],
                                    value[0]['location']['x']),
                                title: data.Direccion,
                              );
                            });
                          }),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        data.Direccion,
                        style: style,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.phone_in_talk,
                              size: 25, color: Colors.blueAccent[100]),
                          onPressed: () async {
                            FlutterPhoneDirectCaller.callNumber(
                                data.Celular.toString());
                          }),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        data.Celular,
                        style: style,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    children: <Widget>[
                      Transition(
                        child: Text('Total:',
                            textAlign: TextAlign.justify,
                            style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.w600)),
                      ),
                      Spacer(),
                      Transition(
                        child: Text('\$' + data.Total,
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w700)),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 80,
                  )
                ],
              )),
        ],
      ),
    );
  }

  List<Widget> _products(context) {
    final data = Provider.of<Orders_Data>(context, listen: false);

    List<dynamic> products = data.Product;
    List<dynamic> price = data.Price;
    List<dynamic> cant = data.Cant;

    List<Widget> prod = [];

    for (int i = 0; i < products.length; i++) {
      final Temp = Row(
        children: <Widget>[
          Text(cant[i] + " ",
              textAlign: TextAlign.justify, style: TextStyle(fontSize: 20)),
          Text(products[i],
              textAlign: TextAlign.justify,
              style: TextStyle(
                  fontSize: 20, color: Colors.black.withOpacity(1.0))),
          Spacer(),
          //   Text(
          //  '\$' + price[i],
          //  style: TextStyle(
          //      color: Colors.black.withOpacity(0.7), fontWeight: FontWeight.w400, fontSize: 20),
          //),
        ],
      );
      prod
        ..add(Temp)
        ..add(
          SizedBox(
            height: 40,
          ),
        );
    }

    return prod;
  }

  void _mostrarAlert(BuildContext context, id) {
    final data = Provider.of<Orders_Data>(context, listen: false);
    CardsProvider post = new CardsProvider();

    showDialog(
        context: context,
        barrierDismissible: false,
        barrierColor: Colors.transparent,
        builder: (context) {
          return FutureBuilder(
            future: post.state(id).then((value) {
              Navigator.pushNamedAndRemoveUntil(
                  context, 'orders', (route) => false);
              data.delete();
            }),
            initialData: [],
            builder: (BuildContext context, snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.active:
                  return Container();
                case ConnectionState.waiting:
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                    ],
                  );
                case ConnectionState.none:
                  return Center(
                    child: Text("no tienes niguna orden"),
                  );
                case ConnectionState.done:
                  return Container(
                    child: Text(''),
                  );
              }
            },
          );
        });
  }
}

class Transition extends StatelessWidget {
  const Transition(
      {Key key,
      this.duration = const Duration(milliseconds: 900),
      @required this.child,
      this.offset = 700.0})
      : super(key: key);
  final Widget child;
  final Duration duration;
  final double offset;

  @override
  Widget build(BuildContext context) {
    return TweenAnimationBuilder<double>(
        child: child,
        tween: Tween(begin: 0.5, end: 0.0),
        duration: duration,
        builder: (context, value, child) {
          return Transform.translate(
            offset: Offset(value * offset, 0.0),
            child: child,
          );
        });
  }
}
