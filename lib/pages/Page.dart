import 'package:flutter/material.dart';
import 'package:food_addresses/pages/Orders_page.dart';
import 'package:food_addresses/pages/log_page.dart';
import 'package:food_addresses/providers/login_provider.dart';
import 'package:food_addresses/providers/notificate_provider.dart';
import 'package:provider/provider.dart';

class Pages extends StatelessWidget {
  const Pages({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final token = Provider.of<Tok>(context, listen: false);
    future(context, token.oid);

    return FutureBuilder(
        future: token.leer(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.active:
              return Container();
            case ConnectionState.waiting:
              return Container();
            case ConnectionState.none:
              return Container();
            case ConnectionState.done:
              if (snapshot.data) {
                return OrdersPage();
              } else {
                return Log();
              }
          }
        });
  }
}
