import 'package:flutter/material.dart';
import 'package:food_addresses/providers/login_provider.dart';
import 'package:food_addresses/widgets/Card_Data.dart';
import 'package:provider/provider.dart';

class OrdersPage extends StatelessWidget {
  const OrdersPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final token = Provider.of<Tok>(context, listen: false);

    return Scaffold(
        appBar: AppBar(
          elevation: 5,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Image.asset(
            'assets/logo.png',
            height: 80,
          ),
          actions: [
            IconButton(
              icon: Icon(Icons.power_settings_new),
              onPressed: () {
                token.Exit();
                Navigator.pushNamed(context, 'log');
              },
              color: Colors.lightBlue[500],
            )
          ],
        ),
        body: Column(
          children: [
            Container(padding: EdgeInsets.only(left: 15,right: 15, top: 15), child: Text(token.name, style: TextStyle(fontSize: 25),)),
            
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.all(15),
                  child: Card_product(),
                ),
              ],
            ),
          ],
        ));
  }
}
