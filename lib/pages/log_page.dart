import 'package:aad_oauth/aad_oauth.dart';
import 'package:aad_oauth/model/config.dart';
import 'package:flutter/material.dart';
import 'package:food_addresses/providers/login_provider.dart';
import 'package:provider/provider.dart';

class Log extends StatelessWidget {
  const Log({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final token = Provider.of<Tok>(context);
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 60),
        decoration: BoxDecoration(
            color: Colors.indigo,
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(250))),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Image.asset('assets/logo.png'),
            ),
            SizedBox(
              height: 40,
            ),
            Container(
                height: 40,
                width: 180,
                padding: EdgeInsets.only(top: 5),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)),
                child: InkWell(
                  onTap: () {
                    token.Islogin(context);
                  },
                  child: Text(
                    'Iniciar sesión',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        color: Colors.indigoAccent),
                  ),
                ))
          ],
        ),
      ),
    );
  }

  Future<void> _loginB2c(context) async  {
    Config configB2C = new Config(
      "16beb467-4973-41c1-857d-da41219757ab",
      "7ff7f425-e0d6-4ba4-9dbd-1cc0a3d171fc",
      "7ff7f425-e0d6-4ba4-9dbd-1cc0a3d171fc offline_access",
      "com.pasiphiku.app://callback",
      isB2C: true,
      azureTenantName: "pasiphikuonline",
      userFlow: "B2C_1_Signin_Signout",
      tokenIdentifier: "UNIQUE IDENTIFIER A",
    );
    AadOAuth oauth = new AadOAuth(configB2C);
     await oauth.login().then((value) async {
       
       Navigator.pushNamed(context, 'orders');
     }
      
     );
  }
  
}
