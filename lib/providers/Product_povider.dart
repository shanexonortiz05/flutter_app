
import 'package:flutter/cupertino.dart';

class Product with ChangeNotifier{

  List<String> _product=[];
  List<String> _cant=[];
  List<String> _price=[];
  String _usuario="";
  String _direccion="";
  String _total="";
  String _fecha="";
  String _celular="";
  String _id="";

  set Produc(String a){
    this._product.add(a);
  }

  get Produc{
    return this._product;
  }

  String ProducP(int a){
    
  if(this._product[a].length>30){
return this._product[a].substring(0,30)+'...';
}else{
return this._product[a];
  }
  }
  set Cant(String a){

    this._cant.add(a);
  }

  String CantP(int a){
    return this._cant[a];
  }

  set Usuario(String a){
    this._usuario=a;
  }

  get Usuario{
    return this._usuario;
  }

  set Direccion(String a){
    this._direccion=a;
  }

  get Direccion{
    return this._direccion;
  }
  
  set Total(String a){
    this._total=a;
  }

  get Total{
    return this._total;
  }

  set Fecha(String a){
    this._fecha=a;
  }

  get Fecha{
    return this._fecha.replaceAll('T', '  ');
  }

  set Celular(String a){
    this._celular=a;
  }

  get Celular{
    return this._celular;
  }

  set Id(String a){
    this._id=a;
  }

  get Id{
    return this._id;
  }

  set Price(String a){
    this._price.add(a);
  }

  String PriceP(int a){
    return this._price[a];
  }

  void restart(){
  _product=[];
   _cant=[];
   _price=[];
   _usuario="";
   _direccion="";
   _total="";
   _fecha="";
   _celular="";
   _id="";
  }

}