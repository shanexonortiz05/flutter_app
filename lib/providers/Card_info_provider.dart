import 'dart:convert';
import 'package:http/http.dart' as http;

class CardsProvider {
  List<dynamic> data = [];

  Future<List<dynamic>> cargar(String oid) async {
    //final resp = await rootBundle.loadString('Data/Cards_Data.json');
    try {
      http.Response response = await http.get(
          'https://dev-pasiphiku-order.azurewebsites.net/api/Order/delivery/{'+oid+'}');
      final deco = json.decode(response.body);
      return deco;
    } catch (e) {}
  }

  Future<String> state(String id) async{
    try{
      print(id);
       final respose=await http.put(
          'https://dev-pasiphiku-order.azurewebsites.net/api/order/'+id+'/status/7');
          return respose.body;
    }catch(e){
      print(e);
    }
  }
}
