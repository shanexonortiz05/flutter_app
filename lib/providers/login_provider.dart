import 'package:aad_oauth/aad_oauth.dart';
import 'package:aad_oauth/model/config.dart';
import 'package:flutter/cupertino.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';


class Tok with ChangeNotifier {
  String _oid = "";
  String _name = "";

  get oid {
    return this._oid;
  }

  get name {
    return this._name;
  }

  void notify(){
    notifyListeners();
  }

  Config configB2C = new Config(
    "16beb467-4973-41c1-857d-da41219757ab",
    "7ff7f425-e0d6-4ba4-9dbd-1cc0a3d171fc",
    "7ff7f425-e0d6-4ba4-9dbd-1cc0a3d171fc offline_access",
    "com.pasiphiku.app://callback",
    isB2C: true,
    azureTenantName: "pasiphikuonline",
    userFlow: "B2C_1_Signin_Signout",
    tokenIdentifier: "UNIQUE IDENTIFIER A",
  );

  Future<void> Islogin(context) async {
    AadOAuth oauth = new AadOAuth(configB2C);
    await oauth.login().then((value) async {
      await decody(oauth);
      _login();
      Navigator.pushNamed(context, 'orders');
    });
  }

  void _login() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool('login', true);
  }

  Future<bool> leer() async {
    final prefs = await SharedPreferences.getInstance();
    bool counter = prefs.getBool('login') ?? false;
    if(counter==true){
      AadOAuth oauth = new AadOAuth(configB2C);
      await decody(oauth);
    }
    return counter;
  }

  Future<void> Exit() async {
    AadOAuth oauth = new AadOAuth(configB2C);
    oauth.logout();
  }

  Future<void> decody(oauth) async {
    String token = await oauth.getIdToken();
    List<String> a = token.toString().split('.');
    String str = a[1];
    String output = str.replaceAll('-', '+').replaceAll('_', '/');
    switch (output.length % 4) {
      // Pad with trailing '='
      case 0: // No pad chars in this case
        break;
      case 2: // Two pad chars
        output += '==';
        break;
      case 3: // One pad char
        output += '=';
        break;
    }

    str = (utf8.decode(base64Url.decode(output)));
    final deco = json.decode(str);
    this._oid = deco['oid'];
    this._name = deco['name'];
  }
}
