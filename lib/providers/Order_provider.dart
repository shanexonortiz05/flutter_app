import 'package:flutter/cupertino.dart';

class Orders_Data with ChangeNotifier {
  List<dynamic> _product = [];
  List<dynamic> _cant = [];
  List<dynamic> _price = [];
  List<String> _usuario = [];
  List<String> _direccion = [];
  List<String> _total = [];
  List<String> _fecha = [];
  List<String> _celular = [];
  List<String> _id = [];
  int _pos = 0;

  void notifi() {
    notifyListeners();
  }

  void Cargar_data(List<dynamic> data) {
    if (data != null) {
      reset();
      List Temp_product = [];
      List Temp_cant = [];
      List Temp_price = [];

      data.forEach((element) {
        this._usuario.add(
            element['customer']['person']['user']['logonUserName'].toString());
        this._direccion.add(element['addressLine1'].toString());
        this._total.add(element['totalDue'].toString());
        this._fecha.add(element['dueDate'].toString());
        this._celular.add(element['phoneNumber'].toString());
        this._id.add(element['salesOrderId'].toString());
        List pro = element['salesOrderDetail'];
        pro.forEach((element) {
          Temp_product.add(element['product']['name'].toString());
          Temp_cant.add(element['orderQty'].toString());
          Temp_price.add(element['product']['listPrice'].toString());
        });
        this._product.add(Temp_product);
        this._cant.add(Temp_cant);
        this._price.add(Temp_price);
      });
    }
  }

  get Product {
    return this._product[_pos];
  }

  get allProducts {
    return this._product.length;
  }

  get Cant {
    return this._cant[_pos];
  }

  get Price {
    return this._price[_pos];
  }

  get Usuario {
    return this._usuario[_pos];
  }

  get Direccion {
    return this._direccion[_pos];
  }

  get Total {
    return this._total[_pos];
  }

  get Fecha {
    return this._fecha[_pos];
  }

  get Celular {
    return this._celular[_pos];
  }

  get Id {
    return this._id[_pos];
  }

  get Pos {
    return this._pos;
  }

  set Pos(int data) {
    this._pos = data;
  }

  void reset() {
    _product = [];
    _cant = [];
    _price = [];
    _usuario = [];
    _direccion = [];
    _total = [];
    _fecha = [];
    _celular = [];
    _id = [];
    _pos = 0;
  }

  void delete(){
    _product.removeAt(_pos);
    _cant.removeAt(_pos);
    _price.removeAt(_pos);
    _usuario.removeAt(_pos);
    _direccion.removeAt(_pos);
    _total.removeAt(_pos);
    _fecha.removeAt(_pos);
    _celular.removeAt(_pos);
    _id.removeAt(_pos);
  }
}
