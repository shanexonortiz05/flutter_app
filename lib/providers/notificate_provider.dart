import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:food_addresses/providers/Card_info_provider.dart';
import 'package:provider/provider.dart';
import 'package:signalr_core/signalr_core.dart';

import 'Order_provider.dart';



Future<void> future(context,oid) async {
  try {
    final data = Provider.of<Orders_Data>(context, listen: false);

    final connection = HubConnectionBuilder()
        .withUrl(
            'https://dev-pasiphiku-order.azurewebsites.net/hubNoti',
            HttpConnectionOptions(
              logging: (level, message) => print(message),
            ))
        .build();

    await connection.start();
    CardsProvider C = new CardsProvider();

    connection.on('notificDelivery', (message) async{
      _showNotifications();
      await C.cargar(oid).then((value){
        data.Cargar_data(value); 
        data.notifi();
      });
    });
  } catch (e) {}

  //await connection.invoke('SendMessage', args: ['Bob', 'Says hi!']);
}

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();
AndroidInitializationSettings androidInitializationSettings;
IOSInitializationSettings iosInitializationSettings;
InitializationSettings initializationSettings;

void initializing() async {
  androidInitializationSettings = AndroidInitializationSettings('app_icon');
  iosInitializationSettings = IOSInitializationSettings(
      onDidReceiveLocalNotification: onDidReceiveLocalNotification);
  initializationSettings = InitializationSettings(
      androidInitializationSettings, iosInitializationSettings);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: onSelectNotification);
}

void _showNotifications() async {
  initializing();
  await notification();
}

void _showNotificationsAfterSecond() async {
  await notificationAfterSec();
}

Future<void> notification() async {
  AndroidNotificationDetails androidNotificationDetails =
      AndroidNotificationDetails('Channel ID', 'Channel title', 'channel body',
          priority: Priority.High, importance: Importance.Max, ticker: 'test');

  IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();

  NotificationDetails notificationDetails =
      NotificationDetails(androidNotificationDetails, iosNotificationDetails);
  await flutterLocalNotificationsPlugin.show(0, 'Orden Asignada',
      'Se le ha asignado nueva orden.', notificationDetails);
}

Future<void> notificationAfterSec() async {
  var timeDelayed = DateTime.now().add(Duration(seconds: 5));
  AndroidNotificationDetails androidNotificationDetails =
      AndroidNotificationDetails(
          'second channel ID', 'second Channel title', 'second channel body',
          priority: Priority.High, importance: Importance.Max, ticker: 'test');

  IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();

  NotificationDetails notificationDetails =
      NotificationDetails(androidNotificationDetails, iosNotificationDetails);
  await flutterLocalNotificationsPlugin.schedule(1, 'Hello there',
      'please subscribe my channel', timeDelayed, notificationDetails);
}

Future onSelectNotification(String payLoad) {
  if (payLoad != null) {
    print(payLoad);
  }
  // we can set navigator to navigate another screen
}

Future onDidReceiveLocalNotification(
    int id, String title, String body, String payload) async {
  return CupertinoAlertDialog(
    title: Text(title),
    content: Text(body),
    actions: <Widget>[
      CupertinoDialogAction(
          isDefaultAction: true,
          onPressed: () {
          },
          child: Text("Okay")),
    ],
  );
}
