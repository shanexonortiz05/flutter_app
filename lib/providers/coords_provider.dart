import 'package:http/http.dart' as http;
import 'dart:convert';

class CoordsAdrres{

  Future<List<dynamic>> coord( address) async {
    //final resp = await rootBundle.loadString('Data/Cards_Data.json');
    try {
      http.Response response = await http.get(
          'https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?f=json&singleLine='+address+', Bogotá, Colombia&outFields=Match_addr,Addr_type');
      final deco = json.decode(response.body);
      List<dynamic> Temp=deco['candidates'];
      //Temp=Temp.first['location'];
      //print(Temp[0]);
      return Temp;
    } catch (e) {
      return [];
    }
  }

}